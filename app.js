const express = require("express");
const morgan = require("morgan");
const personsRouter = require("./routes/personsRouter")

const app = express();

app.use(morgan("dev"))
app.use(express.static("public"))
app.use("/persons", personsRouter)

app.all("*",(req, res)=>{
    res.status(404)
    res.json({message: "server running but don't understand you request:" + req.url})
})

module.exports = app