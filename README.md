# week6testing

Demo uit les week 6 rond unit- en integratietesting met jest en supertest

## Usage

- Om de dependencies te installeren:

```
$ npm install
```

- Nadien kan je het project runnen met:

```
$ npm start
```

- Om de testen te runnen:

```
$ npm test
```



