const request = require("supertest")
const app = require("../app")

describe("Integration tests for gifts API", () => {
    test("GET persons async", async()=> {
        const res = await request(app).get("/persons")
        expect(res.statusCode).toBe(200)
    })
    test("POST person promises", (done) => {
        request(app).post("/persons")
            .send({name: "testperson"})
            .then(res => {
                expect(res.statusCode).toBe(201)
            }).then(done)
    })
})
