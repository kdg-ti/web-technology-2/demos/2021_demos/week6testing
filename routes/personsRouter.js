const express = require("express");
const database = require("../data/db.json")
const personFunctions = require("./personFunctions")

const router = express.Router();

router.use(express.json())

router.get("/", (req, res)=>{
    res.status(200)
    res.json(database.persons);
})

router.get("/:id", (req, res)=>{
    let person = database.persons.find(p=>p.id===parseInt(req.params.id))
    if (person) {
        res.status(200)
        res.json(person);
    } else {
        res.sendStatus(404)
    }

})

router.post("/", (req, res)=>{
    req.body.id = personFunctions.getNextPersonKey(database);
    database.persons.push(req.body);
    personFunctions.assignOthers(database);
    let person = database.persons.find(p=>p.id===req.body.id);
    console.log(person);
    personFunctions.writeDBToFile(database).then(()=>{
        res.status(201)
        res.json(person)
    }).catch(error=>{
        res.status(400).send();
    })
})



module.exports = router;